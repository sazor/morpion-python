class Grille:

     combinaison = [
                ['1.1', '1.2', '1.3'],
                ['2.1', '2.2', '2.3'],
                ['3.1', '3.2', '3.3'],
                ['1.1', '2.1', '3.1'],
                ['1.2', '2.2', '3.2'],
                ['1.3', '2.3', '3.3'],
                ['1.1', '2.2', '3.3'],
                ['1.3', '2.2', '3.1'],
            ]    

     def __init__(self): # 
         self.grid = [
             [' ', ' ', ' '],
             [' ', ' ', ' '],
             [' ', ' ', ' ']
         ]
         
    
     def afficherGrille(self): # affiche la grille
         for i in range(3):
             print(self.grid[i])


     def joueCase(self, case, player):
         index = case.split(".")

         if ( self.grid[int(index[0])-1][int(index[1])-1] != ' '):
             return "existe" # si case déja rempli
         else:

            self.grid[int(index[0])-1][int(index[1])-1] = player
            if (self.verif(player)):
                return "win" # si gagne
            else:    
                return "next" # joueur suivant
                
         
       
     def reset(self): # reset la grille
        for i in range(3):
            for a in range(3):
                self.grid[i][a] = ' '

     def verif(self,symb): #verifie si gagner
         i=0   
         while i < len(self.combinaison):
             index = []

             for a in range(3):
                index.append(self.combinaison[i][a].split("."))
              
             if (self.grid[int(index[0][0])-1][int(index[0][1])-1] == symb and self.grid[int(index[1][0])-1][int(index[1][1])-1] == symb and self.grid[int(index[2][0])-1][int(index[2][1])-1] == symb):
                 return True
             else:    
                i+=1       
          
         return False              


